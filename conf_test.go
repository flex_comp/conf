package conf

import (
	"gitlab.com/flex_comp/comp"
	"testing"
	"time"
)

type cfgExchange struct {
	symbol string `mapstructure:"symbol"`
	fix    int    `mapstructure:"fix"`
	rate   int    `mapstructure:"rate"`
	expire int    `mapstructure:"expire"`
}

type cfgTokenItem struct {
	Contract string       `mapstructure:"contract"`
	Deposit  bool         `mapstructure:"deposit"`
	Withdraw bool         `mapstructure:"withdraw"`
	Exchange *cfgExchange `mapstructure:"exchange"`
}

type cfgNet struct {
	Stream string                   `mapstructure:"stream"`
	Token  map[string]*cfgTokenItem `mapstructure:"token"`
}

type cfgToken struct {
	Net     string  `mapstructure:"net"`
	Mainnet *cfgNet `mapstructure:"mainnet"`
	Kovan   *cfgNet `mapstructure:"kovan"`
}

type model struct {
	TokenInit *cfgToken `mapstructure:"token_init"`
}

func TestConf(t *testing.T) {
	t.Run("test_conf", func(t *testing.T) {
		_ = comp.Init(map[string]interface{}{"config": "./test/test.json"})

		go func() {
			<-time.After(time.Second)
			m := new(model)
			err := Unmarshal(m)
			_ = m.TokenInit
			t.Error(err)
			//t.Log(Get("foo"))     // 直接取值
			//t.Log(Get("in.v1"))   // 嵌套取值
			//t.Log(Get("in.v2.0")) // 取数组元素(.索引)
			//
			//t.Log(util.ToIntSlice(Get("in.v1"))) // 配合util库转需要类型
			//t.Log(util.ToIntSlice(Get("in.v2"))) // 配合util库转需要类型
		}()

		ch := make(chan bool, 1)
		go func() {
			<-time.After(time.Second * 10)
			ch <- true
		}()

		_ = comp.Start(ch)
	})
}
