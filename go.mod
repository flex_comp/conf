module gitlab.com/flex_comp/conf

go 1.16

require (
	github.com/spf13/viper v1.8.1
	gitlab.com/flex_comp/comp v0.1.3
	gitlab.com/flex_comp/util v0.0.0-20210729132803-de11a044b5ed
)
