package conf

import (
	"github.com/spf13/viper"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/util"
	"path"
	"strings"
)

var (
	ins *Conf
)

func init() {
	ins = new(Conf)
	_ = comp.RegComp(ins)
}

type Conf struct {
	dir string
}

func (c *Conf) Init(execArgs map[string]interface{}, args ...interface{}) error {
	if execArgs == nil {
		return ErrNeedPath
	}

	confPath := util.ToString(execArgs["config"])
	dir, file := path.Split(confPath)
	idx := strings.LastIndex(file, ".")
	viper.AddConfigPath(dir)
	viper.SetConfigName(file[:idx])
	viper.SetConfigType(file[idx+1:])
	return viper.ReadInConfig()
}

func (c *Conf) Start(args ...interface{}) error {

	return nil
}

func (c *Conf) UnInit() {
	return
}

func (c *Conf) Name() string {
	return "config"
}

func Get(key string) interface{} {
	return ins.Get(key)
}

func (c *Conf) Get(key string) interface{} {
	return viper.Get(key)
}

func UnmarshalKey(key string, model interface{}) error {
	return ins.UnmarshalKey(key, model)
}

func (c *Conf) UnmarshalKey(key string, model interface{}) error {
	return viper.UnmarshalKey(key, model)
}

func Unmarshal(model interface{}) error {
	return ins.Unmarshal(model)
}

func (c *Conf) Unmarshal(model interface{}) error {
	return viper.Unmarshal(model)
}
