package conf

import "errors"

var (
	ErrNeedPath = errors.New("需要配置文件目录")
)